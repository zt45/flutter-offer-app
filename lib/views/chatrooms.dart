import 'package:offer_app/views/sellerChat.dart';

import '../helper/authenticate.dart';
import '../helper/constants.dart';
import '../helper/helperfunctions.dart';
import '../helper/theme.dart';
import '../services/auth.dart';
import '../services/database.dart';
import '../views/buyerChat.dart';
import '../views/search.dart';
import 'package:flutter/material.dart';

class ChatRoom extends StatefulWidget {
  @override
  _ChatRoomState createState() => _ChatRoomState();
}


class _ChatRoomState extends State<ChatRoom> {
  Stream chatRooms;

  Widget chatRoomsList() {
    return StreamBuilder(
      stream: chatRooms,
      builder: (context, snapshot) {
        return snapshot.hasData
            ? ListView.separated(
            separatorBuilder: (context, index) =>
                Divider(
                  color: Colors.black,
                ),
            itemCount: snapshot.data.documents.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return ChatRoomsTile(
                userName: snapshot.data.documents[index].data['chatRoomId']
                    .toString()
                    .replaceAll('_', '')
                    .replaceAll(Constants.myName, ''),
                chatRoomId:
                snapshot.data.documents[index].data['chatRoomId'],
                declined: snapshot.data.documents[index].data['declined'],
                names:
                List.from(snapshot.data.documents[index].data['users']),
                relation: List.from(
                    snapshot.data.documents[index].data['relation']),
                itemName: snapshot.data.documents[index].data['itemName'],
                itemId: snapshot.data.documents[index].data['itemCode'],
              );
            })
            : Container();
      },
    );
  }

  @override
  void initState() {
    getUserInfogetChats();
    super.initState();
  }

  getUserInfogetChats() async {
    Constants.myName = await HelperFunctions.getUserNameSharedPreference();
    DatabaseMethods().getUserChats(Constants.myName).then((snapshots) {
      setState(() {
        chatRooms = snapshots;
        print(
            'we got the data + ${chatRooms.toString()} this is name  ${Constants
                .myName} ');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Image.network(
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/EBay_logo.svg/800px-EBay_logo.svg.png',
              height: 40,
            ),
            SizedBox(
              width: 20,
            ),
            const Text('Chat Room')
          ],
        ),
        elevation: 0.0,
        centerTitle: false,
        actions: [
          GestureDetector(
            onTap: () {
              AuthService().signOut();
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => Authenticate()));
            },
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Icon(Icons.exit_to_app)),
          )
        ],
      ),
      body: Container(
        child: chatRoomsList(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Search()));
        },
      ),
    );
  }
}

class ChatRoomsTile extends StatelessWidget {
  final String userName;
  final String chatRoomId;
  final String itemName;
  final bool declined;
  final List<String> names;
  final List<String> relation;
  final String itemId;

  ChatRoomsTile({this.userName,
    @required this.chatRoomId,
    this.declined,
    this.names,
    this.relation,
    this.itemName,
    @required this.itemId});

  @override
  Widget build(BuildContext context) {
    bool isSeller = relation[names.indexOf(Constants.myName)] == 'seller';
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                isSeller
                    ? SellerChat(
                  chatRoomId: chatRoomId,
                  userName: userName,
                  declined: declined,
                )
                    : BuyerChat(
                  chatRoomId: chatRoomId,
                  sellerName: userName,
                  declined: declined,
                  itemId:itemId,
                )));
      },
      child: Container(
        color: declined ? Colors.red[200] : Colors.green[200],
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        child: Row(
          children: [
            Expanded(
              child: Column(
                children: [
                  Text('Item: $itemName',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 25,
                          fontFamily: 'RobotoMono',
                          fontWeight: FontWeight.w400)),
                  Text('${relation[names.indexOf(userName)]}: $userName',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black38,
                          fontSize: 20,
                          fontFamily: 'RobotoMono',
                          fontWeight: FontWeight.w400)),
                ],
              ),
            ),
            Container(
              height: 100,
              width: 100,
              color: Colors.black38,
              child: Text(
                'pic goes here',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
